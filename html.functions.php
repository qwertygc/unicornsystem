<?php
function a($array=array(), $label='') {
	foreach($array as $key => $value) {
		$meta .= $key.'="'.$value.'" ';
	}
	return '<a '.$meta.'>'.$label.'</a>'.PHP_EOL;
}
function img($array=array()) {
	foreach($array as $key => $value) {
		$meta .= $key.'="'.$value.'" ';
	}
	return '<img '.$meta.'/>'.PHP_EOL;
}
function meta($type, $array=array()) {
	foreach($array as $key => $value) {
		$meta .= $key.'="'.$value.'" ';
	}
	return '<'.$type.' '.$meta.'/>'.PHP_EOL;
}
function script($array=array(), $label='') {
	foreach($array as $key => $value) {
		$meta .= $key.'="'.$value.'" ';
	}
	return '<script '.$meta.'>'.$label.'</script>'.PHP_EOL;
}
function title($label) {
	return '<title>'.$label.'</title>';
}
function heading($label, $level) {
	return '<h'.$level.'>'.$label.'</h'.$level.'>';
}
