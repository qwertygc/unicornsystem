<?php
function redirect($url) {
	@header('Location: '.$url);
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}
function send_mail($to, $from='no-reply@example.org', $subject = '(No subject)', $message = '') {
	$nl = (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $to)) ? $nl = "\r\n" : "\n";
	$message_html = "<html><head></head><body>".$message."</body></html>";
	$message_txt = strip_tags($message);
	$boundary = "-----=".md5(rand());
	$header = "From: ".$from."".$nl;
	$header.="X-Mailer: PHP/".phpversion();
	$header.= "MIME-Version: 1.0".$nl;
	$header.= "Content-Type: multipart/alternative;".$nl." boundary=\"$boundary\"".$nl;
	$body = $nl."--".$boundary.$nl;
	$body.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$nl;
	$body.= "Content-Transfer-Encoding: 8bit".$nl;
	$body.= $nl.$message_txt.$nl;
	$body.= $nl."--".$boundary.$nl;
	$body.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$nl;
	$body.= "Content-Transfer-Encoding: 8bit".$nl;
	$body.= $nl.$message_html.$nl;
	$body.= $nl."--".$boundary."--".$nl;
	$body.= $nl."--".$boundary."--".$nl;
	mail($to,'=?UTF-8?B?'.base64_encode($subject).'?=',$body,$header);
}
function generateRandomString($length = 10) {
    $characters = '234567ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getRequestProtocol() {
    if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO']))
        return $_SERVER['HTTP_X_FORWARDED_PROTO'];
    else 
        return !empty($_SERVER['HTTPS']) ? "https" : "http";
}
function toslug($string) {
    $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '', ' ' => ''
    );
    $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);
    return strtolower(strtr($string, $table));
}

function int_to_alph($int) {
	$alph = null;
	$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_');
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}


function exportcsv($entries) {
	header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=data.csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	$out = fopen('php://output', 'w');
	foreach ($entries as $entry) {
		fputcsv($out, $entry);
	}
	fclose($out);
	die();
}
function vcard($name='', $email='', $tel=''){
    return utf8_encode('BEGIN:VCARD
        VERSION:4.0
        N:;'.$name.';;;
        FN:'.$name.'
        EMAIL:'.$email.'
        ORG:'.$name.'
        TEL:'.$tel.'
        END:VCARD');
}
