<?php
function dl_lib($local, $source) {
	file_put_contents($local, file_get_contents($source));
}

function erased_dir($dir) {
	if(!is_dir($dir)) {
		throw new InvalidArgumentException("$dir must be a directory");
	}
	if(substr($dir,strlen($dir)-1,1) != '/') {
		$dir .= '/';
	}
	$files = glob($dir.'*',GLOB_MARK);
	if($files) {
		foreach ($files as $file) {
			if(is_dir($file)) {
				erased_dir($file);
			} 
			else {
				unlink($file);
			}
		}
	}
	rmdir($dir);
}

function recurseCopy($src, $dst) {
	$dir = opendir($src);
	mkdir($dst);
	while(false !== ( $file = readdir($dir)) ) {
		if (( $file != '.' ) && ( $file != '..' )) {
			if ( is_dir($src . '/' . $file) ) {
				recurseCopy($src . '/' . $file, $dst . '/' . $file);
			}
			else {
			 	copy($src . '/' . $file,$dst . '/' . $file);
			}
		}
	}
	closedir($dir);
}

function Size($path) { #http://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
    $bytes = sprintf('%u', filesize($path));
    if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            $size = sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }

    return array('bytes'=>$bytes, 'size'=>$size);
}

function folderSize($path) { #https://gist.github.com/eusonlito/5099936
	$bytes = 0;
	foreach (glob(rtrim($path, '/').'/*', GLOB_NOSORT) as $each) {
		$bytes += is_file($each) ? filesize($each) : folderSize($each);
	}
	 if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            $size = sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }
   return array('bytes'=>$bytes, 'size'=>$size);
}

function ZipDir($filename, $path) {
	$rootPath = realpath($path);
	$zip = new ZipArchive();
	$zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
	);

	foreach ($files as $name => $file) {
		if (!$file->isDir()) {
			$filePath = $file->getRealPath();
			$relativePath = substr($filePath, strlen($rootPath) + 1);
			$zip->addFile($filePath, $relativePath);
		}
	}
	$zip->close();
}
