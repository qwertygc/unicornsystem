<?php
namespace Unicorn;
class table {
	
	function __construct($metadata=array()) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$table ='';
		foreach($metadata as $key => $value) {
			$table .= $key.'="'.$value.'" ';
		}
		echo '<table '.$table.'>'.PHP_EOL;
	}
	function head($array, $trmd=array(), $thmd=array()) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$tr ='';
		$th ='';
		foreach($trmd as $key => $value) {
			$tr .= $key.'="'.$value.'" ';
		}
		foreach($thmd as $key => $value) {
			$th .= $key.'="'.$value.'" ';
		}
		echo '<tr '.$tr.'>'.PHP_EOL;
		foreach($array as $value) {
			echo '<th '.$th.'>'.$value.'</th>'.PHP_EOL;
		}
		echo '</tr>'.PHP_EOL;
	}
	function row($array, $trmd=array(), $tdmd=array()) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$tr ='';
		$td ='';
		foreach($trmd as $key => $value) {
			$tr .= $key.'="'.$value.'" ';
		}
		foreach($tdmd as $key => $value) {
			$td .= $key.'="'.$value.'" ';
		}
		echo '<tr '.$tr.'>'.PHP_EOL;
		foreach($array as $value) {
			echo '<td '.$td.'>'.$value.'</td>'.PHP_EOL;
		}
		echo '</tr>'.PHP_EOL;
	}

	function endtable() {
		echo "</table>".PHP_EOL;
	}
	function __destruct() {
    }
	
}
