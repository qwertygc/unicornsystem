<?php
namespace Unicorn;

class Atom {
	function __construct() {
		echo '<?xml version="1.0" encoding="utf-8"?>'.PHP_EOL;
		echo '<feed xmlns="http://www.w3.org/2005/Atom">'.PHP_EOL;
	}
	function title($entry) {
		echo '<title>'.$entry.'</title>'.PHP_EOL;
	}
	function subtitle($entry) {
		echo '<subtitle>'.$entry.'</subtitle>'.PHP_EOL;
	}
	function uuid() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',mt_rand(0,0xffff),mt_rand(0,0xffff),mt_rand(0,0xffff),mt_rand(0,0x0fff)|0x4000,mt_rand( 0,0x3fff)|0x8000,mt_rand( 0, 0xffff ),mt_rand(0,0xffff), mt_rand(0,0xffff));
	}
	function link($entry) {
		echo '<link href="'.$entry.'"/>'.PHP_EOL;
	}
	function id($entry) {
		echo '<id>'.$entry.'</id>'.PHP_EOL;
	}
	function startentry() {
		echo '<entry>'.PHP_EOL;
	}
	function endentry() {
		echo '</entry>'.PHP_EOL;
	}
	function published($entry) {
		echo '<published>'.date(DATE_ATOM, $entry).'</published>'.PHP_EOL;
	}
	function updated($entry) {
		echo '<updated>'.date(DATE_ATOM, $entry).'</updated>'.PHP_EOL;
	}
	function content($entry) {
		echo '<content type="html"><![CDATA['.$entry.']]></content>'.PHP_EOL;
	}
	function summary($entry) {
		echo '<summary type="html"><![CDATA['.$entry.']]></summary>'.PHP_EOL;
	}
	function author($entry) {
		echo '<author><name>'.$entry.'</name></author>'.PHP_EOL;
	}
	function __destruct() {
		echo '</feed>';
    }
}
