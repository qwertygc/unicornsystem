<?php
namespace Unicorn;
class lessdb {
	public $dbname;
	function __construct($dbname) {
		$this->dbname =  (isset($dbname)) ? $dbname : 'data.db';
		if(file_exists($this->dbname)) {
		}
		else {
			file_put_contents($this->dbname, array());
		}
		return $this->dbname;
	}
	function namedb() {
		return $this->dbname;
	}
	function read() {
		return unserialize(gzinflate(base64_decode(substr(file_get_contents($this->dbname),strlen('<?php /*'),-strlen('*/ ?>')))));
	}
	function put($array) {
		$dbarray = $this->read();
		$dbarray[] = $array;
		file_put_contents($this->dbname, '<?php /*'.base64_encode(gzdeflate(serialize($dbarray))).'*/ ?>');
	}
	function delete($key) {
		$dbarray = $this->read();
		if(array_key_exists($key, $this->read())) {
			unset($dbarray[$key]);
		file_put_contents($this->dbname, '<?php /*'.base64_encode(gzdeflate(serialize($dbname))).'*/ ?>');
		}
	}
	function modify($key, $array) {
		$dbarray = $this->read();
		$dbarray[$key] = $array;
		file_put_contents($this->dbname, '<?php /*'.base64_encode(gzdeflate(serialize($dbname))).'*/ ?>');
	}
	function __destruct() {
    }
	
}
