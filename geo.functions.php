<?php
function geocodage($loc) {
$options = array(
	'http'=>array(
		'method'=>"GET",
		'header'=>"Accept-language: en\r\n".
				  "Cookie: foo=bar\r\n".
				"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\r\n")
		);
		$geocodage = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search/?q='.$loc.'&format=json&addressdetails=1&limit=1', false,  stream_context_create($options)), true);
		$lat = round($geocodage[0]['lon'], 3);
		$lon =  round($geocodage[0]['lat'], 3);
		return array('lat'=>$lat, 'lon'=>$lon);
}
function geocodage_reverse($lat, $lon) {
	$options = array(
	'http'=>array(
		'method'=>"GET",
		'header'=>"Accept-language: en\r\n".
				  "Cookie: foo=bar\r\n".
				"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\r\n")
		);
		$geocodage = json_decode(file_get_contents('https://nominatim.openstreetmap.org/reverse.php?lat='.$lat.'&lon='.$lon.'&zoom=20&format=json', false,  stream_context_create($options)), true);
	return $geocodage['display_name'];
}

