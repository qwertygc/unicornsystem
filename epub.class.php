<?php
namespace Unicorn;
function create_epub(	$nameepub='Book', 
						$metadata=array('title'=>'Hello World: My First EPUB',
										'creator'=>'My Name',
										'identifier'=>'urn:uuid:0cc33cbd-94e2-49c1-909a-72ae16bc2658',
										'language'=>'en-US',
										'date'=>'34903434',
										'rights'=>'Copyright'),
						$manifest = array (
										array('id'=>'ncx', 'href'=>'toc.ncx', 'media-type'=>'application/x-dtbncx+xml'),
										array('id'=>'title.html', 'href'=>'title.html', 'media-type'=>'application/xhtml+xml'),
										array('id'=>'content.html', 'href'=>'content.html', 'media-type'=>'application/xhtml+xml'),
										array('id'=>'cover.png', 'href'=>'cover.png', 'media-type'=>'image/png'),
										array('id'=>'stylesheet.css', 'href'=>'stylesheet.css', 'media-type'=>'text/css')
						),
						$spine = 	array(
										'title.html',
										'content.html'
									),
						$toc = 		array (
										array('text'=>'Book cover', 'content'=>'title.html'),
										array('text'=>'Contents', 'content'=>'content.html')
									),
						$files = 	array (
										array('file'=>'title.html', 'content'=>'<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Hello World: My First EPUB</title><link type="text/css" rel="stylesheet" href="stylesheet.css" /></head><body><h1>Hello World: My First EPUB</h1><div><img src="cover.png" alt="Title page"/></div></body></html>'),
										array('file'=>'content.html', 'content'=>'<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Hello World: My First EPUB</title><link type="text/css" rel="stylesheet" href="stylesheet.css" /></head><body><h1>Hello World</h1><div><p>Hello</p></div></body></html>'),
										array('file'=>'stylesheet.css', 'content'=>'body {font-family: sans-serif;     }h1,h2,h3,h4 {font-family: serif;     color: red;}'),
										array('file'=>'cover.png', 'content'=>'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAABNCAIAAADLiSCBAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAYkSURBVHhe7dbRlqMgEEXR+f+f7ukFh9UQTcQoaunZTzN1QdOGKvPvR5IkqYM/GiRJUhd/NEiSpC7+aJCk1f4Nww2kS/KAStI5+JmQUJKuzZMqSQfhB0JBVYrDUytJo/DroKAqheUhlqR98NOgQiDdhWda6zALN+BCUnAc6AqBdF+ecn3CLKwQbMCF1mCndB7OYoVAehLPvWYwFC8zFvk0u+LS0hxOSYVAejY7QY0T52O+9QjcQHqDg1IhkNSyN/Rn91mZ528n9kiDceAqBJKW2C36s2V65uH7gkw6D2exQiBpPftHjZ6RmifvCzLpJBzECWJJe7Cj1Hg3ZPP8zShJh+MIzmGFpJHsNL16GcEv/5VGy0duFiskncQm1DyGtGNaY3C85rBC0vXYn5pRz+787yxXpE6cmzmskBSKravG54Ge01/8X8/GaXiPdZLuwq7Wq55xn9f84v+6F77dJayW9Bi2veZ1vhLyyyOjpAvjq1rCaklqOR30Fi+QCsEbLEoo6Sg89yWslqSvOES0Am+eCeIKQUJJX+EhLmG1JI3krNEOeHFVCBJKCaVn41n0YY8kXYAjSUPwxqtM67kSGn/JSmyWpGicXzoIL8zipZLXnIjPsRKbb+fGf5qkLRwNOkd+6c5ixbe4ykpsVuIDkTTL0aCryC/vF+/qH+SraQsfo6RZjgadKb/m+7FN4/m0JU05FzREfscvYnUf9rTINICPV9ILh4LWya/qRawehtvMYYX24COVVHMc6E9+Q3zG0ivhk33EUn3FBygpcxY8Qn5xLmJ1cPwxHdigDj4xSb+cApeWJ/V2XO7BeBAd2KA5PKOEkqQnsfNXY2QegltqDJ5yBzaowqPx4UhPcreGZ4yNxJ10U3zNHdjweDyOhJKkmzqnyRkwA3ADaW+csCWsfiqegp0o3dQ3vc1U2IALScFxoDuw4TH4sxNKkuJr+pkWX8JqSXPokzXYeVP8kY4OKT6aOaMmaW/0WIUgoTSHFbfAn+SokcKye6Vz8P4sqE4Qv8GiaPj0/nqQorFppUvgLVpQXcLqOay4Nj6rvx6kIOxV6Yp4lxZU12DnGyy6DD5WQknS9difUgC8TguqG3ChCeJT8VH89SBdj80pxUPfFlT3wBVbZIfj9g4o6TLoRlqzyEVJIdC3BdX9cN0KwVG4q6NJOtt8E9KgCSVJQdC6BdVdcekW2UjcybkknWS59+jRgqqkIGjdguoA3KBFtjeunlCSNN7qfqNNE0qS4qB7C6rDcJsW2U64aEJJ0hibeow2LahKioPuLagOxs0qBJtxuYSSpP3s2Vd0akJJUig0cEF1PO7XIvsWV0koSdpmVC/RqQklSdHQwwXVo3DXFtlKbHYcSdsc0UI0a0FVUjT0cIXgQNy4QtCHPQklSd1O7nlKkmKik1tkR+GuLbKPWJpQkvTRya1Cv1YIJIVFM1cIDsSNKwRvsCihJGnicu1B104QSwqINm6RHYW7tshaZI4daSJMV9DELTJJAdHGLbKjcNcWWZtSkp4tdifQzS0ySQHRxi2yo3DXyksxL5Me6Iann7aeIJYUDT1cITgKd53DCukZHnTiafEWmaRQaOAW2VG4a4VAuq+nn3J6vUUmKRQauEV2CG7ZIpNuwQM9g16fIJYUB91bIRiMm73BIikaz+4KtHuLTFIQtG6LbABukFBymCgsj+lWtHuLTFIQtG6LbD9cN6FUIWiRSdfgiRyCdp8glhQBfTtBvA3XSijNYUWLTDqch+9QdHyLTFIQtO4E8XrsTyh9xNIWmTSS5+x8dHyFQFIoNPAEcR/2JJT6sKdFJu3EI3VFtHuLTFI09HCL7COWFlTXYGeFQPqKBygMOr5CICkg2niCeA4rKgRrsLNCIHXwuARGx7fIJMVEJ08Qt8gqBGuws6AqzfF83A19XyGQFBn9PEFcUK0QdGNbhUDyR8MT0PctMknB0dIVgoJqhaAb2wqqeiS//oei+1tkkiKjnwuqFYIKQR/2VAj0AH7Z+sMAqBBICotmLqi2yCoEfdhTUNUd+e3qE2ZAi0xSQLRxQXWCuELQgQ0VAsXnd6nVGAMVAknR0MMF1TmsaJEtYfUEsaL4+fkPJ9UnSUoZL9oAAAAASUVORK5CYII=')
									)
					) {
	// array('title'=>'', 'creator'=>'', 'identifier'=>'', 'language'=>'', 'date'=>'', 'right'=>'');
	mkdir($nameepub, 0775);
	file_put_contents($nameepub.'/mimetype', 'application/epub+zip');
	mkdir($nameepub.'/META-INF', 0775);
	file_put_contents($nameepub.'/META-INF/container.xml', '<?xml version="1.0"?>
<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
  <rootfiles>
    <rootfile full-path="OEBPS/content.opf"
     media-type="application/oebps-package+xml" />
  </rootfiles>
</container>');
	mkdir($nameepub.'/OEBPS', 0775);
	$contentopt = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<package xmlns="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/"  unique-identifier="bookid" version="2.0">
  <metadata>
    <dc:title>'.$metadata['title'].'</dc:title>
    <dc:creator>'.$metadata['creator'].'</dc:creator>
    <dc:identifier id="bookid">'.$metadata['identifier'].'</dc:identifier>
    <dc:language>'.$metadata['language'].'</dc:language>
	<dc:date>'.date('c', $metadata['date']).'</dc:date>
	<dc:rights>'.$metadata['rights'].'</dc:rights>
    <meta name="cover" content="cover-image" />
  </metadata>
  <manifest>';
  foreach($manifest as $item) {
	    $contentopt .= '<item id="'.$item['id'].'" href="'.$item['href'].'" media-type="'.$item['media-type'].'"/>';
	}
 $contentopt .= '</manifest>
  <spine toc="ncx">';
  foreach($spine as $s) {
	$contentopt .= '<itemref idref="'.$s.'" />';
  }
 $contentopt .= '</spine>
  <guide />
</package>';
$tocfile = '<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN"
                 "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
  <head>
    <meta name="dtb:uid" content="'.$metadata['identifier'].'"/>
    <meta name="dtb:depth" content="1"/>
    <meta name="dtb:totalPageCount" content="0"/>
    <meta name="dtb:maxPageNumber" content="0"/>
  </head>
  <docTitle>
    <text>'.$metadata['identifier'].'</text>
  </docTitle>
  <navMap>';
 foreach($toc as $id => $name) {
	 $tocfile .= '<navPoint id="navpoint-'.$id.'" playOrder="'.$id.'">
      <navLabel>
        <text>'.$name['text'].'</text>
      </navLabel>
      <content src="'.$name['content'].'"/>
    </navPoint>';
 }
$tocfile .='  </navMap>
</ncx>';
	file_put_contents($nameepub.'/OEBPS/content.opf', $contentopt);
	file_put_contents($nameepub.'/OEBPS/toc.ncx', $tocfile);
	foreach($files as $file) {
		file_put_contents($nameepub.'/OEBPS/'.$file['file'], $file['content']);
	}
}
